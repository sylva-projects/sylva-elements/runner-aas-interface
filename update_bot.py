#!/usr/bin/env python3

import argparse
import requests
import sys
from datetime import date, timedelta

"""
This script can be used to create or renew all tokens needed for runner-aas management.

It takes as parameter a personal access token with sylva-projects owner rights

It does the following:
* Renew runner-bot account in 'ci-tooling' group
* Recreate associated trigger token in 'runner-aas-interface' project
* Set variable in allowed projet
* Create runner management access token in each allowed projet and set token as variable in runner-aas project
"""

BOT_ACCOUNT_NAME = "runner-bot"
CI_TOOLING_GROUP = "sylva-projects%2Fsylva-elements%2Fci-tooling"
RUNNER_AAS_INTERFACE_PROJECT = f"{CI_TOOLING_GROUP}%2Frunner-aas-interface"
RUNNER_AAS_PROJECT = f"{CI_TOOLING_GROUP}%2Frunner-aas"

ALLOWED_PROJECTS = {
    "sylva-projects%2Fsylva-core",
    "sylva-projects%2Fsylva-elements%2Fkiwi-imagebuilder",
}

today = date.today()
validity_days = 350
expire_date = (today + timedelta(days=validity_days)).strftime("%Y-%m-%d %H:%M:%S")


def get_bot_access_token_id(account_name, owner_token):
    url = f"https://gitlab.com/api/v4/groups/{CI_TOOLING_GROUP}/access_tokens"
    headers = {"PRIVATE-TOKEN": owner_token}
    r = requests.get(url, headers=headers)
    if r.status_code != 200:
        print("Unable to retrieve access tokens")
        print(r)
        sys.exit(1)
    json_response = r.json()
    for element in json_response:
        if element["name"] == account_name:
            return element["user_id"]
    print(f"Unable to find account {account_name}")
    sys.exit(1)


def delete_bot_account(account_name, owner_token):
    account_id = get_bot_access_token_id(account_name, owner_token)
    url = f"https://gitlab.com/api/v4/groups/{CI_TOOLING_GROUP}/access_tokens/{account_id}"
    headers = {"PRIVATE-TOKEN": owner_token}
    r = requests.delete(url, headers=headers)
    if r.status_code == 204:
        print(f"Account {account_name} deleted")
        return
    if r.status_code == 404:
        print(f"Account {account_name} not found")
        return
    print(f"Unable to delete account {account_name}")
    sys.exit(1)


def create_bot_account(account_name, owner_token):
    url = f"https://gitlab.com/api/v4/groups/{CI_TOOLING_GROUP}/access_tokens"
    headers = {"PRIVATE-TOKEN": owner_token}
    json = {
        "name": account_name,
        "scopes": ["api"],
        "expires_at": expire_date,
        "access_level": 40,
    }
    print(json)
    r = requests.post(url, headers=headers, json=json)
    json_response = r.json()
    if "token" in json_response:
        return json_response["token"]
    print(f"Unable to create account {account_name}")
    print(r)
    print(json_response)
    sys.exit(1)


def generate_trigger_token(bot_token):
    url = f"https://gitlab.com/api/v4/projects/{RUNNER_AAS_INTERFACE_PROJECT}/triggers"
    headers = {"PRIVATE-TOKEN": bot_token}
    form = {"description": "runner-aas trigger token"}
    r = requests.post(url, headers=headers, data=form)
    json_response = r.json()
    if "token" in json_response:
        return json_response["token"]
    print("Unable to create trigger token")
    print(r)
    print(json_response)
    sys.exit(1)


def get_trigger_tokens(token):
    url = f"https://gitlab.com/api/v4/projects/{RUNNER_AAS_INTERFACE_PROJECT}/triggers"
    headers = {"PRIVATE-TOKEN": token}
    r = requests.get(url, headers=headers)
    return r.json()


def clean_trigger_token(token):
    trigger_tokens = get_trigger_tokens(token)
    for tk in trigger_tokens:
        url = f"https://gitlab.com/api/v4/projects/{RUNNER_AAS_INTERFACE_PROJECT}/triggers/{tk['id']}"
        headers = {"PRIVATE-TOKEN": token}
        requests.delete(url, headers=headers)


def set_trigger_token_variable(trigger_token, owner_token):
    for project in ALLOWED_PROJECTS:
        set_variable(owner_token, project, "RUNNER_AAS_TOKEN", trigger_token, protected=False, masked=True, description="Trigger token for runner aas")


def get_access_token_id(project, name, owner_token):
    url = f"https://gitlab.com/api/v4/projects/{project}/access_tokens?expires_at={expire_date}"
    headers = {"PRIVATE-TOKEN": owner_token}
    tokens = requests.get(url, headers=headers).json()
    for tk in tokens:
        if tk["name"] == name and not tk["revoked"]:
            return tk["id"]


def create_or_rotate_runner_mgmt_access_tokens(owner_token):
    token_name = "Equinix runner management"
    for project in ALLOWED_PROJECTS:
        token_id = get_access_token_id(project, token_name, owner_token)
        url = f"https://gitlab.com/api/v4/projects/{project}/access_tokens"
        headers = {"PRIVATE-TOKEN": owner_token}
        json = {
            "name": token_name,
            "scopes": ["create_runner", "manage_runner", "read_api"],
            "expires_at": expire_date,
            "access_level": 40,
        }

        if token_id:  # rotate
            r = requests.post(f"{url}/{token_id}/rotate", headers=headers, json=json)
            if r.status_code == 200:
                print(f"Runner management token rotated in {project}")
            else:
                print(f"Unable to rotate runner management token in {project}")
                print(r)
                print(f"{url}/{token_id}/rotate")
                sys.exit(1)

        else:  # create
            r = requests.post(url, headers=headers, json=json)
            if r.status_code == 201:
                print(f"Runner management token set in {project}")
            else:
                print(f"Unable to set runner management token in {project}")
                print(r)
                sys.exit(1)
        token = r.json()["token"]
        runner_mgmt_variable_name = f"{project.upper().replace('-', '_').split('%2F')[-1]}_RUNNER_MGMT_TOKEN"
        set_variable(owner_token, RUNNER_AAS_PROJECT, runner_mgmt_variable_name, token)


def set_variable(owner_token, project, variable_name, value, protected=True, masked=True, description=""):
    url = f"https://gitlab.com/api/v4/projects/{project}/variables/{variable_name}"
    headers = {"PRIVATE-TOKEN": owner_token}
    form = {
        "variable_type": "env_var",
        "key": variable_name,
        "value": value,
        "protected": protected,
        "masked": masked,
        "description": description,
    }
    status = ""
    r = requests.put(url, headers=headers, data=form)
    if r.status_code == 200:
        status = "success"
    elif r.status_code == 404:
        url = f"https://gitlab.com/api/v4/projects/{project}/variables"
        r = requests.post(url, headers=headers, data=form)
        if r.status_code == 201:
            status = "success"

    if status == "success":
        print(f"Variable {variable_name} set in {project}")
    else:
        print(f"Unable to set variable {variable_name} in {project}")
        print(r)
        sys.exit(1)


if __name__ == "__main__":
    parser = argparse.ArgumentParser(description="Runner aaS bot account renewal")
    parser.add_argument("token", help="Gitlab token of a sylva-projects owner")
    args = parser.parse_args()
    user_owner_token = args.token

    # Delete / Recreate bot account
    print("# Delete current bot account")
    delete_bot_account(BOT_ACCOUNT_NAME, user_owner_token)
    print("# Recreate bot account")
    bot_token = create_bot_account(BOT_ACCOUNT_NAME, user_owner_token)

    # Generate trigger token in runner-aas-interface repo
    print("# Clean trigger token")
    clean_trigger_token(bot_token)
    print("# Regenerate trigger token")
    trigger_token = generate_trigger_token(bot_token)

    # set trigger token as variable in allowed projects
    print("# Set CI variables")
    set_trigger_token_variable(trigger_token, user_owner_token)

    # Generate runner creation token (sylva-projects wide)
    create_or_rotate_runner_mgmt_access_tokens(user_owner_token)
