# Runner aaS interface

This repo is used to safely trigger pipeline in `https://gitlab.com/sylva-projects/sylva-elements/runner-aas`

```mermaid
graph LR
    A[sylva-core] --> |triggers pipeline| B
    B[runner-aas-interface] --> |triggers pipeline| C[runner-aas]
```

## Purpose

[Runner aaS project](https://gitlab.com/sylva-projects/sylva-elements/ci-tooling/runner-aas) enables the creation of on demand Gitlab runners into 3rd party infrastructure for allowed projects developers in CI pipelines. It handles various credentials needed for interacting with  gitlab and 3rd party APIs which should not be accessible to non-maintainer sylva developers.

This repository is just storing a pipeline configuration which triggers a pipeline into [runner-aas](https://gitlab.com/sylva-projects/sylva-elements/ci-tooling/runner-aas) in order to make sure `runner-aas` pipeline is launched in a proper way (especially without any unwanted environment variables).

## Usage

In order to make sure, pipeline configuration in this repo (`runner-aas-interface`) is not modified, is it not possible for non maintainer users to push code or create branch here (all branches are protected).

By default it is not possible to a non-maintainer user to launch pipeline here. That's why allowed projects are provisioned with a [trigger token](https://docs.gitlab.com/ee/ci/triggers/index.html) allowing to create a pipeline in behalf of a dedicated bot account allowed to run here.

### Currently allowed projects to request runners

- sylva-core
- sylva-elements/kiwi-imagebuilder

### Currently available infrastructures

- Equinix
